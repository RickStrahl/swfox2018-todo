import {Injectable, Component, OnInit, Input} from '@angular/core';
import {Response} from "@angular/http";
import {Observable, throwError}  from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class ErrorInfo {
    constructor() {        
    }

    message: string;
    response: HttpErrorResponse;     

    // Creates consistent response with a `message` property
    // and re-throws an observable exception
    parseObservableResponseError(response:HttpErrorResponse): Observable<any> {        
        let err = new ErrorInfo();
        err.response = response;  // original response
        err.message = response.message || response.statusText;
        
        // Response contains JSON that has error info
        if (response.error && response.error.message) {
            err = response.error;
            err["response"] = response;
            return throwError(err);
        }
                        
        if (!err.message)
            err.message = "Unknown server failure.";

        return throwError(err);        
    }
}
