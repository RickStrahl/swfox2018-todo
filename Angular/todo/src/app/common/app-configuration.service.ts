import { Injectable } from '@angular/core';


declare var toastr: any;

@Injectable({
  providedIn: 'root'
})
export class AppConfiguration {
  
  urls = {
    baseUrl: "./",                          // deployed in site
    //baseUrl: "http://localhost/todo/",    // debug
    todos: "todos.todo",
    todo: "todo.todo",
    toggleTodo: "toggleTodo.todo",

    // config.urls.url("todos")
    url: (name, parm1?, parm2?, parm3?) => {
      var url = this.urls.baseUrl + this.urls[name];
      if (parm1)
        url += "/" + parm1;
      if (parm2)
        url += "/" + parm2;
      if (parm3)
        url += "/" + parm3;

      return url;
    }
  };

  constructor(){            
    this.setToastrOptions();
    
    if(location.port && (location.port == "3000") || (location.port== "4200") )
      this.urls.baseUrl = "http://localhost/todo/"; // debug local
    
    //this.urls.baseUrl = "http://localhost:5001/"; // kestrel
    //this.urls.baseUrl = "http://localhost:26448/"; // iis Express
    //this.urls.baseUrl = "http://localhost/albumviewer/"; // iis          
    //this.urls.baseUrl = "https://albumviewer.west-wind.com/";  // online          
  }


  setToastrOptions() {
    toastr.options.closeButton = true;
    toastr.options.positionClass = "toast-bottom-right";
  }
}