import { Component, OnInit, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit, OnDestroy  {
  title = 'the Southwest Fox Todo Application';
  name = "Rick";
  time = new Date();
  timerInterval = null;

  constructor() {
  
  }

  message() {
    return this.name + " - " + new DatePipe("en_US").transform(this.time,"MMM dd, yyyy HH:mm:ss");
  }

  ngOnInit(){
    this.timerInterval  = setInterval( ()=> {
      this.time = new Date();
    },1000);
  }



  ngOnDestroy(){
    clearInterval(this.timerInterval)
  }

  updateTime() {
    this.time = new Date();
  }
  
}
