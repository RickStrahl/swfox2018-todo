import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppConfiguration } from '../common/app-configuration.service';
import { TodoItem } from './todos.component';
import { ErrorInfo } from '../common/error-info';

/* @
	Wraps all service calls in order to:
	- provide internal hooks for caching data
	- provide consistent error information
	- in the form of an error object
@ */
@Injectable()
export class TodoService {

	todos: TodoItem[] = [];

	constructor(private http: HttpClient,
		private config: AppConfiguration) {
	}

	getTodos(): Observable<any> {
		
		return this.http.get<TodoItem[]>(this.config.urls.baseUrl + "todos.todo")
			.pipe(
				map((todos) => {					
					// intercept requests
					this.todos = todos;
					return todos;
				}),
				catchError( new ErrorInfo().parseObservableResponseError)								
			);

		// // rethrow the error as something easier to work
		// // with. Normally you'd return an error object
		// // with parsed error info
		// return Observable.throw("Failed to get todo items.");
	}

	addTodo(todo: TodoItem): Observable<boolean> {
		return this.http.put<boolean>(this.config.urls.url("todo"), todo)
			.pipe(
				catchError(new ErrorInfo().parseObservableResponseError)
			);
	}

	toggleTodo(todo: TodoItem): Observable<boolean> {
		return this.http.put<boolean>(this.config.urls.url("toggleTodo"), todo)
			.pipe(
				catchError(new ErrorInfo().parseObservableResponseError)
			);
	}

	removeTodo(todo: TodoItem): Observable<boolean> {
		return this.http.delete<boolean>(this.config.urls.url("todo") + "?title=" + todo.title)
			.pipe(
				catchError(new ErrorInfo().parseObservableResponseError)
			);
	}
}