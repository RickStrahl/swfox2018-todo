import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfiguration } from '../common/app-configuration.service';

import {Observable} from "rxjs";
import {map, catchError} from "rxjs/operators";
import { ErrorInfo } from '../common/error-info';

import { TodoService } from './todoService';


declare var toastr:any;

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos:TodoItem[]  = null; 
  activeTodo = new TodoItem();

  constructor(private todoService:TodoService, 
              private http:HttpClient, 
              private config:AppConfiguration) { 
  }

  ngOnInit() {
    this.loadTodosWeb();
  }




  loadTodos() {
    this.loadTodosWeb();
  }

  loadTodosWeb(){
    // service
    this.todoService.getTodos()
      .subscribe((todos)=> {        
        this.todos = todos;
      },
      (error)=> {                 
        toastr.error("Couldn't retrieve todos: " + error.message);
      });      

    // direct access
    // this.http.get<TodoItem[]>(this.config.urls.url("todos"))
    //         .subscribe((todos)=> {
    //             this.todos = todos;
    //         },(error)=> {              
    //           toastr.error("couldn't get todo items");
    //         });
  } 

  private loadTodosInternal() {
    this.todos = [
      {
        id: "t1",
        title: "SWFox Angular Presentation",
        description: "Try to show up on time this time",
        completed: false,
        entered: new Date()
      },
      {
        id: "t2",
        title: "Do FoxPro presentation",
        description: "Should go good, let's hope I don't fail...",
        completed: false,
        entered: new Date()
      },
      {
        id: "t3",
        title: "Do raffle at the end of day one",
        description: "Should go good, let's hope I don't fail...",
        completed: false,
        entered: new Date()
      },
      {
        id: "t4",
        title: "Arrive at conference",
        description: "Arrive in Phoenix and catch a cab to hotel",
        completed: true,
        entered: new Date()
      }
    ];   
  }

  
  toggleCompleted(todo) {
    var origState = todo.completed;
    todo.completed = !todo.completed;

    this.http.post<boolean>(this.config.urls.url("toggleTodo"),todo)
            .subscribe((result)=> {
              // do nothing UI auto-updates
            },
            (error)=> {
              todo.completed = origState;
              toastr.error("couldn't update todo.")
             });
  }

  addTodo(todo,form) {
  
    // service
    this.todoService.addTodo(todo)
      .subscribe((result)=> {
        this.todos.unshift(todo);
        
        // reset the form to empty
        this.activeTodo = new TodoItem();          
        form.reset();
        
        toastr.success("Todo item added.");
      },(error)=> {                 
        toastr.error("Couldn't add todo: " + error.message);
      });   

    // direct
    // this.http.post<TodoItem>(this.config.urls.url("todo"),
    //         this.activeTodo)
    //       .subscribe((td)=> {
    //          // insert the item              
    //          this.todos.unshift(todo);

    //          // reset the form to empty
    //          this.activeTodo = new TodoItem();  
             
    //          toastr.success("Todo item added.");
    //       },(error) => toastr.error("Failed to add todo item."));

    // memory
    // this.todos.unshift(todo);
    // this.activeTodo = new TodoItem();
  }

  removeTodo(todo) {
    // service
    this.todoService.removeTodo(todo)
      .subscribe(
        (result) => {
          toastr.success("Todo deleted...");
          this.todos = this.todos.filter((td) => td.title != todo.title);
        },
        (error) => {
          toastr.error("Couldn't delete todo: " + error.message);
        }
      );  
    

    // direct
    // this.http.delete<boolean>(this.config.urls.url("todo") + "?title=" + todo.title)
    //         .subscribe((result)=> {
    //           toastr.success("Todo deleted...");
    //           this.todos = this.todos.filter((td)=> td.title != todo.title)
    //         },
    //         (error)=> { 
    //           toastr.error("Todo couldn't be deleted..."); 
    //         });

    // memory
    //this.todos = this.todos.filter((td)=> td.title != todo.title)    
  }


}

export class TodoItem {
  id = "";
	title:string = null;
	description:string = null;
  completed:boolean = false;
  entered = new Date(); 
}