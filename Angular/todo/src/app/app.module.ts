import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about/about.component';
import { TodosComponent } from './todos/todos.component';
import { AppConfiguration } from './common/app-configuration.service';
import { TodoService } from './todos/todoService';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    TodosComponent
    
  ],
  imports: [
    BrowserModule,    
    AppRoutingModule,

    FormsModule,
    HttpClientModule,    
  ],
  providers: [
    AppConfiguration,
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
}
