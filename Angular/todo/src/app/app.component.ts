import { Component, OnDestroy, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  
  ngOnInit() {

    // have to manually enable/disable tabs
    $("#MainTabs li a").click(function (e) {      
      $("#MainTabs li a").removeClass("active");        
      $(this).addClass("active");
  });
  }
}

