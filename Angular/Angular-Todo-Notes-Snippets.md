### Angular CLI

```
ng new todo --routing --skip-tests
```


### Angular Startup Imports

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Add these
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,    
    AppRoutingModule,
    
    // and these
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```


### Route Setup

```ts
import {Routes} from '@angular/router';
import {About} from './about/about';
import {Home} from './home/home';
import {Todo} from "./todo/todo";

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'todo', pathMatch: 'full'},
  {path: 'home', component: Home},
  {path: 'about', component: About},
  {path: 'todo', component: Todo }
];
```

### TodoItem class

```typescript
export class TodoItem {
	id = "";
	title:string = null;
	description:string = null;
	completed:boolean = false;
	entered = new Date(); 
}
```

### Initial Todo Items

```ts
todos:TodoItem[]  = [
this.todos = [
      {
        id: "t1",
        title: "SWFox Angular Presentation",
        description: "Try to show up on time this time",
        completed: false,
        entered: new Date()
      },
      {
        id: "t2",
        title: "Do FoxPro presentation",
        description: "Should go good, let's hope I don't fail...",
        completed: false,
        entered: new Date()
      },
      {
        id: "t3",
        title: "Do raffle at the end of day one",
        description: "Should go good, let's hope I don't fail...",
        completed: false,
        entered: new Date()
      },
      {
        id: "t4",
        title: "Arrive at conference",
        description: "Arrive in Phoenix and catch a cab to hotel",
        completed: true,
        entered: new Date()
      }
    ];   
```

### Todo CSS

```ts
form input:not(.ng-pristine).ng-invalid,
select:not(.ng-pristine).ng-invalid, textarea:not(.ng-pristine).ng-invalid
{
    background: lightpink;
    border-left: 5px solid red;
}
form input:not([type=submit]):not([type=checkbox]):not([type=radio]):not([type=file]):not(.ng-pristine).ng-valid,
select:not(.ng-pristine).ng-invalid, textarea:not(.ng-pristine):not(.ng-untouched).ng-valid
{
    background: #adfbdc;
    border-left: 5px solid green;
}

.todo-item {
    padding: 5px;
    border-bottom: 1px solid silver;
    transition: opacity 900ms ease-out;
}
.todo-header {
    font-weight: bold;
    font-size: 1.2em;
    color: #457196;
}
.todo-content {
    padding-left: 30px;
}
.todo-content .fa-check {
    color: green !important;
    font-weight: bold;
    font-size: 1.2em;
}
.completed {
    text-decoration: line-through;
    font-style: italic;
    opacity: 0.4;
}
```

### External var references

```ts
declare var $: any;
declare var toastr: any;
```